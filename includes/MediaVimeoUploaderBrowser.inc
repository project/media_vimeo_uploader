<?php
/**
 * @file
 * Definition of MediaVimeoUploaderBrowser.
 */

/**
 * Media browser plugin for Vimeo Video Uploader.
 *
 * Class MediaVimeoUploaderBrowser.
 */
class MediaVimeoUploaderBrowser extends \MediaBrowserPlugin {

  /**
   * {@inheritdoc}
   */
  public function access($account = NULL) {
    return media_internet_access($account);
  }

  /**
   * {@inheritdoc}
   */
  public function view() {
    global $base_root;

    // Display 2nd step with video information.
    $query = drupal_get_query_parameters();
    $options = array();

    if (!empty($query['video_uri'])) {
      $options['video_uri'] = $query['video_uri'];

      return array(
        'form' => drupal_get_form('media_vimeo_uploader_upload_form', 2, $options),
      );
    }

    // Get upload link for form action.
    $response = media_vimeo_uploader_library_get()->request('/me/videos', array(
      'type' => 'POST',
      'redirect_url' => $base_root . request_uri(),
      'link' => '',
      // @todo need to be updated for PRO account.
      'upgrade_to_1080' => FALSE,
    ), 'POST');

    if (201 !== $response['status'] || empty($response['body']['upload_link_secure'])) {
      watchdog('plp_video_uploader', 'Vimeo upload form error: @error', array('@error' => json_encode($response['body'])), WATCHDOG_ERROR);

      return array(
        '#markup' => t('Internal vimeo error. Please check !link.', array(
          '!link' => l(t('log message'), 'admin/reports/dblog'),
        )),
      );
    }

    $options['upload_link'] = $response['body']['upload_link_secure'];

    return array(
      'form' => drupal_get_form('media_vimeo_uploader_upload_form', 1, $options),
    );
  }

}
