<?php
/**
 * @file
 * Vimeo related functions.
 */

/**
 * Get the instance of Vimeo.php library.
 *
 * @return \Vimeo\Vimeo
 *   Instance of the Vimeo.php library.
 *
 * @throws \Exception
 */
function media_vimeo_uploader_library_get() {
  // @todo Remove conditions when patch will be merged.
  // @see https://www.drupal.org/node/1884246
  if (!libraries_load('vimeo.php')) {
    throw new \Exception(t('vimeo.php could not be loaded.'));
  }

  return new \Vimeo\Vimeo(
    media_vimeo_uploader_client_id_get(),
    media_vimeo_uploader_client_secret_get(),
    media_vimeo_uploader_access_token_get()
  );
}

/**
 * Vimeo uploader form.
 */
function media_vimeo_uploader_upload_form(array $form, array &$form_state, $step, array $options) {
  switch ($step) {
    case 1:
      media_vimeo_uploader_file_form($form, $form_state, $options);
      break;

    case 2:
      media_vimeo_uploader_info_form($form, $form_state, $options);
      break;
  }

  return $form;
}

/**
 * Vimeo uploader file form.
 */
function media_vimeo_uploader_file_form(array &$form, array &$form_state, array $options) {
  if (!empty($options['upload_link'])) {
    $form['#action'] = $options['upload_link'];
  }

  $form['file_data'] = array(
    '#name' => 'file_data',
    '#type' => 'file',
    '#title' => t('Choose a file'),
    '#description' => t('Select video file that will be uploaded to Vimeo.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );
}

/**
 * Vimeo uploader info form.
 */
function media_vimeo_uploader_info_form(array &$form, array &$form_state, array $options) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
    '#required' => TRUE,
    '#description' => t('Video title'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('Video description'),
  );

  $form['video_uri'] = array(
    '#type' => 'value',
    '#value' => empty($options['video_uri']) ? '' : $options['video_uri'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#id' => 'edit-next',
  );

  $form['#submit'][] = 'media_vimeo_uploader_info_form_submit';
}

/**
 * Vimeo uploader info form submit.
 */
function media_vimeo_uploader_info_form_submit(array &$form, array &$form_state) {
  $values = $form_state['values'];

  list(,, $video_id) = explode('/', $values['video_uri']);

  if (empty($video_id)) {
    form_error($form['video_uri'], t('URI "%uri" is incorrect. An unknown error occurred.', array(
      '%uri' => $values['video_uri'],
    )));
  }

  $response = media_vimeo_uploader_library_get()->request("/videos/$video_id", array(
    'name' => $values['title'],
    'description' => $values['description'],
  ), 'PATCH');

  // Check if everything ok and we have link to vimeo vide.
  if ($response['status'] !== 200 || empty($response['body']['link'])) {
    watchdog('media_vimeo_uploader', 'Vimeo update video error: @error', array(
      '@error' => json_encode($response['body']),
    ), WATCHDOG_ERROR);

    form_error($form['video_uri'], t('Internal Vimeo error. Please check the: !link.', array(
      '!link' => l(t('log message'), 'admin/reports/dblog'),
    )));

    return;
  }

  $video_url = $response['body']['link'];

  try {
    $file = media_internet_get_provider($video_url)->save();

    if (empty($file->fid)) {
      throw new \InvalidArgumentException(t('The file with "%url" URL could not be saved. An unknown error occurred.', array(
        '%url' => $video_url,
      )));
    }

    // Save file via file_entity module.
    if (!module_load_include('inc', 'file_entity', 'file_entity.pages')) {
      throw new \RuntimeException(t('The "File Entity" module could not be loaded.'));
    }

    // Set form to uploading step (1).
    // @see file_entity_add_upload()
    $form['#step'] = 1;
    // Allow to load the file via file_load().
    $form_state['storage']['upload'] = $file->fid;

    file_entity_add_upload_submit($form, $form_state);
  }
  catch (\Exception $e) {
    form_error($form['video_uri'], $e->getMessage());
  }
}

/**
 * Get Vimeo API Client ID.
 *
 * @return string
 *   Client ID.
 */
function media_vimeo_uploader_client_id_get() {
  return variable_get('media_vimeo_uploader_client_id', '');
}

/**
 * Get Vimeo API Client Secret.
 *
 * @return string
 *   Client secret key.
 */
function media_vimeo_uploader_client_secret_get() {
  return variable_get('media_vimeo_uploader_client_secret', '');
}

/**
 * Get Vimeo API Access Token.
 *
 * @return string
 *   Access token.
 */
function media_vimeo_uploader_access_token_get() {
  // @todo this may be dynamic variable.
  return variable_get('media_vimeo_uploader_access_token', '');
}

/**
 * Get download URL for Vimeo video.
 *
 * @param string $video_uri
 *   Video URI.
 *
 * @return string
 *   URL for downloading the video.
 */
function media_vimeo_uploader_vimeo_download_url($video_uri) {
  $response = media_vimeo_uploader_library_get()
    ->request(sprintf('/videos/%s', media_vimeo_uploader_extract_video_id($video_uri)));

  // Enable link to download file:
  // 1. Link is available(PRO account)
  // 2. Is public video.
  // 3. Video is already processed.
  if (
    200 == $response['status'] &&
    !empty($response['body']['download'][0]['link']) &&
    TRUE == $response['body']['privacy']['download'] &&
    'available' == $response['body']['status']
  ) {
    return $response['body']['download'][0]['link'];
  }

  return '';
}

/**
 * Returns video ID.
 *
 * @param string $uri
 *   Video URI.
 *
 * @return string
 *   Video ID.
 */
function media_vimeo_uploader_extract_video_id($uri) {
  $parts = file_stream_wrapper_get_instance_by_uri($uri)->get_parameters();

  return empty($parts['v']) ? '' : $parts['v'];
}
